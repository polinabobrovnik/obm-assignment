import { fetchMembersList, fetchDetailedMember } from '../utils';
import { URL } from '../utils/constants';

export const setMembers = members => ({
    type: 'SET_MEMBERS',
    members
});

export const updateMembers = () => dispatch => fetchMembersList(URL)
    .then(members => dispatch(setMembers(members)));

export const setDetailedMember = member => ({
    type: 'SET_DETAILED_MEMBER',
    member
});

export const updateDetailedMember = (memberId) => dispatch => fetchDetailedMember(URL, memberId)
    .then(detailedMember => dispatch(setDetailedMember(detailedMember)));

export const selectSortOption = selectedOptionId => ({
    type: 'SELECT_SORT_OPTION',
    selectedOptionId
});

export const selectFilter = filterName => ({
    type: 'SELECT_FILTER',
    filterName
});

export const setSearchQuery = searchQuery => ({
    type: 'SET_SEARCH_QUERY',
    searchQuery
});

export const toggleDeleteMode = () => ({ type: 'TOGGLE_DELETE_MODE' });

export const includeToRemoveList = memberId => ({
    type: 'INCLUDE_TO_REMOVE_LIST',
    memberId
});

export const excludeFromRemoveList = memberId => ({
    type: 'EXCLUDE_FROM_REMOVE_LIST',
    memberId
});

export const deleteMembers = membersToRemove => ({
    type: 'DELETE_MEMBERS',
    membersToRemove
});

export const toggleEditMode = () => ({
    type: 'TOGGLE_EDIT_MODE',
});

export const updateEditModel = model => ({
    type: 'UPDATE_EDIT_MODEL',
    model
});

export const confirmEdit = model => ({
    type: 'CONFIRM_EDIT',
    model
});
