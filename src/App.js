import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateMembers } from './actions';
import Members from './components/Members';
import DetailedMember from './components/DetailedMember';
import { Wrapper } from './components/Styled';

class App extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(updateMembers());
  }

  render() {
    return (
      <Wrapper>
        <Route exact path="/" component={Members} />
        <Route path="/member/:memberId" component={DetailedMember} />
      </Wrapper>
    );
  }
}

export default withRouter(connect()(App));