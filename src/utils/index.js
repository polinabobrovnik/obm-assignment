import membersList from "../mock/members.json"
import detailedMembersList from "../mock/detailedMember.json"

export const fetchMembersList = (url) => {
    //return fetch(url).then(response => response.json());
    return new Promise((res, rej) => {
        res(membersList);
    });
};

export const fetchDetailedMember = (url, memberId) => {
    //return fetch(url).then(response => response.json());
    return new Promise((res, rej) => {
        res(detailedMembersList[memberId]);
    });
};

export const getFullName = member => `${member.firstName}${member.infix ? ` ${member.infix} ` : ` `}${member.lastName}`;

export const search = (members, searchQuery) => members.filter(member => {
    const fullName = getFullName(member).toLowerCase();
    const lcSearchQuery = searchQuery.toLowerCase();
    return fullName.includes(lcSearchQuery) || member.companyName.toLowerCase().includes(lcSearchQuery);
});

export const filter = (members, filterBy) => filterBy 
    ? members.filter(member => member.jobTitle === filterBy) 
    : members;

export const sort = (members, sortBy) => [...members].sort((a, b) => {
    if (a[sortBy.field] > b[sortBy.field]) {
        return sortBy.ascending;
    }
    if (a[sortBy.field] < b[sortBy.field]) {
        return -1 * sortBy.ascending;
    }
    return 0;
});
