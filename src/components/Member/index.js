import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { includeToRemoveList, excludeFromRemoveList } from '../../actions';
import { getFullName } from '../../utils';
import { MemberWrapper, StyledLink, MemberInfoWrapper, OrangeSpan, Checkbox } from '../Styled';

const Member = ({
    firstName, infix, lastName, companyName, jobTitle, isInDeleteMode,
    membersToRemove, id, includeToRemove, excludeFromRemove
}) => (
        <MemberWrapper>
        {isInDeleteMode && (<>
        <Checkbox>
            <input 
                type="checkbox"
                checked={membersToRemove.includes(id)}
                onChange={
                    (event) => event.target.checked ? includeToRemove(id) : excludeFromRemove(id)
                } 
            />
        </Checkbox>
        </>)}
        <StyledLink to={`/member/${id}`}>
            <MemberInfoWrapper>
                <div><OrangeSpan>Full name:</OrangeSpan> {getFullName({ firstName, infix, lastName })} </div> 
                <div><OrangeSpan>Company Name:</OrangeSpan> {companyName}</div>
                <div><OrangeSpan>Job Title:</OrangeSpan> {jobTitle}</div>
            </MemberInfoWrapper>
            
        </StyledLink>
        </MemberWrapper>
);

const mapStateToProps = state => ({
    isInDeleteMode: state.isInDeleteMode,
    membersToRemove: state.membersToRemove
});

const mapDispatchToProps = dispatch => ({
    includeToRemove: memberId => dispatch(includeToRemoveList(memberId)),
    excludeFromRemove: memberId => dispatch(excludeFromRemoveList(memberId)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Member));