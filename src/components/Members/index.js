import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ControlPanel from '../ControlPanel';
import Member from '../Member';
import { search, filter, sort } from '../../utils'
import { MembersContainer } from '../Styled';

const Members = ({ members, filterBy, sortBy, searchQuery }) => {
    const aggregatedMembers = sort(search(filter(members, filterBy), searchQuery), sortBy);
    return (
        <>
            <ControlPanel />
            <MembersContainer>
                {aggregatedMembers.map(member => (<Member key={member.id} {...member} />))}
            </MembersContainer>
        </>
    )
};

const mapStateToProps = state => ({
    members: state.members,
    sortBy: state.sort.selectedOption,
    filterBy: state.filter,
    searchQuery: state.searchQuery
});

export default withRouter(connect(mapStateToProps)(Members));