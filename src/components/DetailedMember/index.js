import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateDetailedMember, toggleEditMode, updateEditModel, confirmEdit } from '../../actions';
import {
    Wrapper,
    Avatar,
    DetailedMemberWrapper,
    CompanyLogo,
    DetailedMemberInfo,
    CompanyWrapper,
    CompanyInfo,
    Button
} from '../Styled';

class DetailedMember extends React.Component {
    constructor(props) {
        super(props);
        this.onEditClick = this.onEditClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        const { updateDetailedMember, match } = this.props;
        updateDetailedMember(match.params.memberId);
    }

    onEditClick() {
        if (!this.props.editMode) {
            this.props.updateEditModel(this.props.memberInfo);
        }
        this.props.toggleEditMode();
    }

    handleInputChange(event) {
        this.props.updateEditModel({
            [event.target.name]: event.target.value
        });
    }

    render() {
        const {
            firstName,
            infix,
            lastName,
            companyName,
            jobTitle,
            photo,
            companyPhoto,
            companyAddress
        } = this.props.memberInfo;
        return (
            <Wrapper>
            <Button onClick={this.onEditClick}>{this.props.editMode ? 'Cancel' : 'Edit'}</Button>
            {this.props.editMode ?
            (<>
                <Button onClick={() => {
                    this.props.confirmEdit(this.props.model);
                    this.props.toggleEditMode();
                }}>Apply</Button>
                            <form>
                <DetailedMemberWrapper>
                                <Avatar src={photo} alt={lastName} />
                                <DetailedMemberInfo>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.firstName}
                                        name='firstName'
                                        placeholder='First Name'
                                    ></input>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.infix}
                                        name='infix'
                                        placeholder='Infix'
                                    ></input>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.lastName}
                                        name='lastName'
                                        placeholder='Last Name'
                                    ></input>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.jobTitle}
                                        name='jobTitle'
                                        placeholder='Job Title'
                                    ></input>
                                
                                <CompanyLogo src={companyPhoto} alt={companyName} />
                                <CompanyInfo>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.companyName}
                                        name='companyName'
                                        placeholder='Company Name'
                                    ></input>
                                    <input
                                        onChange={this.handleInputChange}
                                        value={this.props.model.companyAddress}
                                        name='companyAddress'
                                        placeholder='Company Address'
                                    ></input>
                                </CompanyInfo>
                                </DetailedMemberInfo>
                </DetailedMemberWrapper>
                            </form>
            </>) : 
            (<DetailedMemberWrapper>
                <Avatar src={photo} alt={lastName} />
                <DetailedMemberInfo>
                    <div>{firstName} {infix} {lastName}</div>
                    <div>{jobTitle}</div>
                    <CompanyWrapper>
                        <CompanyLogo src={companyPhoto} alt={companyName} />
                        <CompanyInfo>
                            <span>{companyName}</span>
                            <span>{companyAddress}</span>
                        </CompanyInfo>
                    </CompanyWrapper>
                </DetailedMemberInfo>                
            </DetailedMemberWrapper>)
            }
            </Wrapper>
        );
    }
}

const mapStateToProps = state => ({
    memberInfo: state.detailedMember,
    editMode: state.editMember.editMode,
    model: state.editMember.model
});

const mapDispatchToProps = dispatch => ({
    updateDetailedMember: memberId => dispatch(updateDetailedMember(memberId)),
    toggleEditMode: () => dispatch(toggleEditMode()),
    updateEditModel: model => dispatch(updateEditModel(model)),
    confirmEdit: model => dispatch(confirmEdit(model)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DetailedMember));