import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  max-width: 1200px;
  margin: auto;
`;

export const SortWrapper = styled.div`
  display: flex;
  width: 200px;
  justify-content: space-between;
`;

export const MemberInfoWrapper = styled.div`
  background-color: #F6F6F6;
  color: black;
  border: 1px dotted black;
  padding: 5px 10px;
  margin: 10px;
  width: 400px;
`;

export const StyledLink = styled(Link)`
    text-decoration: none;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export const OrangeSpan = styled.span`
    color: #FF8900;
    font-style: italic;
`;

export const MemberWrapper = styled.div`
    display: flex;
`;

export const Checkbox = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const MembersContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

export const ControlPanelWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 20px;
`;

export const SearchWrapper = styled.div`
  flex-grow: 2;
`;

export const SearchInput = styled.input`
  width: 100%;
`;

export const Avatar = styled.img`
  border-radius: 50%;
  width: 200px;
  height: 200px;
  object-fit: cover;
`;

export const DetailedMemberWrapper = styled.div`
    display: flex;
    margin-top: 30px;
`;

export const CompanyLogo = styled.img`
  width: 70px;
  height: 70px;
  margin-right: 15px;
`;

export const DetailedMemberInfo = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 30px;
    justify-content: center;
`;

export const CompanyWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

export const CompanyInfo = styled.div`
    display: flex;
    align-items: start;
    justify-content: space-between;
    flex-direction: column;
`;

export const Button = styled.button`
  box-shadow:inset 0px 1px 0px 0px #fce2c1;
  background:linear-gradient(to bottom, #ffc477 5%, #fb9e25 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25',GradientType=0);
  background-color:#ffc477;
  border-radius:6px;
  border:1px solid #eeb44f;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:12px;
  padding:3px 14px;
  text-decoration:none;
  text-shadow:0px 1px 0px #cc9f52;

  &:hover {
    background:linear-gradient(to bottom, #fb9e25 5%, #ffc477 100%);
    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fb9e25', endColorstr='#ffc477',GradientType=0);
    background-color:#fb9e25;
  }
  &:active {
    position:relative;
    top:1px;
  }
  &:disabled {
    background: #ffe9cc;
  }
`;
