import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { selectSortOption } from '../../actions';
import { SortWrapper } from '../Styled';

const Sort = ({ options, selectedOption, selectOption }) => (
    <SortWrapper>
        <label>Sort by</label>
        <select value={selectedOption.id} onChange={(event) => selectOption(event.target.value)}>
            {options.map(option => (
                <option key={option.id} value={option.id}>{option.name}</option>
            ))}
        </select>
    </SortWrapper>
);

const mapStateToProps = state => ({
    options: state.sort.options,
    selectedOption: state.sort.selectedOption,
});

const mapDispatchToProps = dispatch => ({
    selectOption: optionId => dispatch(selectSortOption(optionId)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sort));