import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setSearchQuery } from '../../actions';
import { SearchWrapper, SearchInput } from '../Styled';

const Search = ({ searchQuery, setSearch }) => (
    <SearchWrapper>
        <SearchInput value={searchQuery} onChange={(event) => setSearch(event.target.value)}></SearchInput>
    </SearchWrapper>
);

const mapStateToProps = state => ({
    searchQuery: state.searchQuery,
});

const mapDispatchToProps = dispatch => ({
    setSearch: searchQuery => dispatch(setSearchQuery(searchQuery)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search));