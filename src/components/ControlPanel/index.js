import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Sort from './Sort';
import Search from './Search';
import Filter from './Filter';
import { toggleDeleteMode, deleteMembers } from '../../actions';
import { ControlPanelWrapper, Button } from '../Styled';

const ControlPanel = ({ isInDeleteMode, toggleDeleteMode, membersToRemove, deleteMembers }) => (
    <ControlPanelWrapper>
        <Sort />
        <Search />
        <Filter />
        <Button onClick={toggleDeleteMode}>{isInDeleteMode ? 'CANCEL DELETION' : 'DELETE MEMBERS'}</Button>
        {isInDeleteMode && (
            <Button 
                disabled={membersToRemove.length === 0}
                onClick={() => deleteMembers(membersToRemove)}
            >CONFIRM DELETION</Button>)}
    </ControlPanelWrapper>
);

const mapStateToProps = state => ({
    isInDeleteMode: state.isInDeleteMode,
    membersToRemove: state.membersToRemove
});

const mapDispatchToProps = dispatch => ({
    toggleDeleteMode: () => dispatch(toggleDeleteMode()),
    deleteMembers: (membersToRemove) => dispatch(deleteMembers(membersToRemove)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ControlPanel));