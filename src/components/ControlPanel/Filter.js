import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { selectFilter } from '../../actions';

const Filter = ({ members, selectFilter }) => {
    const jobTitles = new Set(members.map(member => member.jobTitle));
    return (
        <select onChange={(event) => selectFilter(event.target.value)}>
            <option value="">No Filter</option>
            {Array.from(jobTitles).map(jobTitle => (
                <option key={jobTitle} value={jobTitle}>{jobTitle}</option>
            ))}
        </select>
    );
};

const mapStateToProps = state => ({
    members: state.members,
});

const mapDispatchToProps = dispatch => ({
    selectFilter: filter => dispatch(selectFilter(filter)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Filter));