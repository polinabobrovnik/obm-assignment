const members = (state = [], action) => {
    switch (action.type) {
        case 'SET_MEMBERS':
            return action.members;
        case 'DELETE_MEMBERS':
            return state.filter(member => !action.membersToRemove.includes(member.id));
        case 'CONFIRM_EDIT':
            return state.map(member => member.id === action.model.id ? action.model : member);
        default:
            return state;
    }
};

export default members;