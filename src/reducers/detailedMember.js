const detailedMember = (state = {}, action) => {
    switch (action.type) {
        case 'SET_DETAILED_MEMBER':
            return action.member;
        case 'CONFIRM_EDIT':
            return action.model;
        default:
            return state;
    }
};

export default detailedMember;