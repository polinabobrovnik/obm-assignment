import { combineReducers } from 'redux';
import members from './members';
import sort from './sort';
import filter from './filter';
import searchQuery from './searchQuery';
import deleteMode from './deleteMode';
import membersToRemove from './membersToRemove';
import detailedMember from './detailedMember';
import editMember from './edit';

export default combineReducers({
    members,
    sort,
    filter,
    searchQuery,
    isInDeleteMode: deleteMode,
    membersToRemove,
    detailedMember,
    editMember
});