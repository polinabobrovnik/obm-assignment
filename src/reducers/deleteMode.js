const deleteMode = (state = false, action) => {
    switch (action.type) {
        case 'TOGGLE_DELETE_MODE':
            return !state;
        default:
            return state;
    }
};

export default deleteMode;