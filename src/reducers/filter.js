const filter = (state = '', action) => {
    switch (action.type) {
        case 'SELECT_FILTER':
            return action.filterName;
        default:
            return state;
    }
};

export default filter;