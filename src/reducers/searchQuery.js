const searchQuery = (state = '', action) => {
    switch (action.type) {
        case 'SET_SEARCH_QUERY':
            return action.searchQuery;
        default:
            return state;
    }
};

export default searchQuery;