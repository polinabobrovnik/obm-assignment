const membersToRemove = (state = [], action) => {
    switch (action.type) {
        case 'INCLUDE_TO_REMOVE_LIST':
            return [...state, action.memberId];
        case 'EXCLUDE_FROM_REMOVE_LIST':
            return state.filter(idToRemove => idToRemove !== action.memberId);
        case 'DELETE_MEMBERS':
            return [];
        default:
            return state;
    }
};

export default membersToRemove;