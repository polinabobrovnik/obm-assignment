const initialState = {
    options: [
        {
            id: 'firstNameA',
            field: 'firstName',
            name: 'First Name A-Z',
            ascending: 1
        },
        {
            id: 'firstNameZ',
            field: 'firstName',
            name: 'First Name Z-A',
            ascending: -1
        },
        {
            id: 'lastNameA',
            field: 'lastName',
            name: 'Last Name A-Z',
            ascending: 1
        },
        {
            id: 'lastNameZ',
            field: 'lastName',
            name: 'Last Name Z-A',
            ascending: -1
        },
        {
            id: 'companyNameA',
            field: 'companyName',
            name: 'Company Name A-Z',
            ascending: 1
        },
        {
            id: 'companyNameZ',
            field: 'companyName',
            name: 'Company Name Z-A',
            ascending: -1
        },
        {
            id: 'jobTitleA',
            field: 'jobTitle',
            name: 'Job Title A-Z',
            ascending: 1
        },
        {
            id: 'jobTitleZ',
            field: 'jobTitle',
            name: 'Job Title Z-A',
            ascending: -1
        },
    ],
    selectedOption: {
        id: 'firstName',
        field: 'firstName',
        name: 'First Name A-Z',
        ascending: 1
    },
};

const sort = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECT_SORT_OPTION':
            return {
                ...state,
                selectedOption: state.options.find(option => option.id === action.selectedOptionId)
            };
        default:
            return state;
    }
};

export default sort;