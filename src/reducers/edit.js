const initialState = {
    editMode: false,
    model: {}
};
const editMember = (state = initialState, action) => {
    switch (action.type) {
        case 'TOGGLE_EDIT_MODE':
            return { ...state, editMode: !state.editMode };
        case 'UPDATE_EDIT_MODEL':
            return { ...state, model: { ...state.model, ...action.model } };
        default:
            return state;
    }
};

export default editMember;